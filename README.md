# Pixi project template

```bash
pip install pre-commit
pre-commit install

cd code

# Install pixi
curl -fsSL https://pixi.sh/install.sh | bash

# Install all dependencies
pixi install

# Activate a shell in the environment
pixi shell
# or source activate .pixi/envs/default

# Activate pre-commit
pre-commit install

# (Optional): launch jupyter notebook if not using vscode
jupyter notebook
```
