import re
from pathlib import Path

log_file = Path(".") / "main.log"
img_path = Path("img")

re_img_filename = re.compile(r"<(img\/.*), id=.*,")

with open(log_file, "r", encoding="iso-8859-1") as fp:
    log = fp.read()

used_images = {Path(i) for i in re.findall(re_img_filename, log)}
all_images = set(img_path.glob("*"))
to_remove = all_images.difference(used_images)

for i in to_remove:
    i.unlink()
