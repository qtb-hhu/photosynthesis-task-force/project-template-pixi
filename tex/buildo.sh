#!/bin/bash
# expects input like "main.tex"

NAME=$(echo $1$ | (cut -d "." -f 1))

bash ./build.sh $1
xdg-open "$NAME.pdf"
